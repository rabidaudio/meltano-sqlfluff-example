with _src_regions as (
  select * from {{ source('tap_carbon', 'region') }}
),

_src_entries as (
  select * from {{ source('tap_carbon', 'entry') }}
),

_england_region_id as (
  select id from _src_regions where dnoregion = 'England' limit 1
),

_england_entries_by_time_of_day as (
  select
    _src_entries.forecast,
    _src_entries."from"::time as time_of_day
  from _src_entries
  inner join _england_region_id on _england_region_id.id = _src_entries.region_id
),

avg_daily_england_intensity as (
  select
    time_of_day,
    avg(forecast) as avg_intensity
  from _england_entries_by_time_of_day
  group by time_of_day
  order by time_of_day asc
)

select * from avg_daily_england_intensity
