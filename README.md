This is an example project for setting up SQLFluff as a linter for DBT transforms within a Meltano project.

If you've already got Meltano and DBT set up, skip to [Configure SQLFluff](#configure-sqlfluff).

# Steps to reproduce

## [Setup Meltano](https://gitlab.com/rabidaudio/meltano-sqlfluff-example/-/commit/d684dea7f7376a4fd1f8d501cf1b9f1c140f2001)

```bash
python3.7 -m venv .venv
source .venv/bin/activate
pip install --upgrade pip
pip install meltano
meltano init meltano-sqlfluff-example
cd meltano-sqlfluff-example

# Create an EL pipeline and run
meltano add extractor tap-carbon-intensity
meltano add loader target-postgres
createdb meltano-sqlfluff-example
echo 'CREATE SCHEMA tap_carbon;' | psql meltano-sqlfluff-example
meltano config target-postgres set postgres_database meltano-sqlfluff-example
meltano schedule add pipeline tap-carbon-intensity target-postgres '@daily'
meltano schedule run pipeline
```

## [Configure DBT](https://gitlab.com/rabidaudio/meltano-sqlfluff-example/-/commit/00c08f547ba9158606f8c43353b4b4652e182adb)

```bash
meltano add transformer dbt
echo "
PG_ADDRESS=localhost
PG_PORT=5432
PG_USERNAME=$(whoami)
PG_PASSWORD=
PG_DATABASE=meltano-sqlfluff-example
" > .env
# <create files in transform/models/carbon/>
meltano elt tap-carbon-intensity target-postgres --transform=only
DBT_TARGET=postgres meltano invoke dbt test
```

## [Configure SQLFluff](4c7a265c2d898094cdb823d86742a9e4bd3cdb5a)

```bash
meltano add --custom utility sqlfluff
# (pip_url) [sqlfluff]: sqlfluff[dbt]==0.6.5
# (executable) [sqlfluff[dbt]==0.6.5]: sqlfluff
echo '.meltano/' > .sqlfluffignore
echo '
target/
dbt_modules/
macros/' > transform/.sqlfluffignore
echo '
[sqlfluff]
dialect = postgres
templater = dbt
output_line_length = 80
ignore_templated_areas = True
runaway_limit = 100

[sqlfluff:templater:dbt]
project_dir = transform
profiles_dir = transform/profile
profile = meltano
target = postgres

# Feel free to change these as you like:

[sqlfluff:rules]
tab_space_size = 2
max_line_length = 120
indent_unit = space
comma_style = trailing

[sqlfluff:rules:L014]
extended_capitalisation_policy = lower
' > .sqlfluff

echo 'DBT_TARGET=postgres
DBT_TARGET_SCHEMA=public
' >> .env
meltano invoke sqlfluff lint -v
meltano invoke sqlfluff fix
```

# Quirks

1. You have to install dbt along with sqlfluff by adding it to the pip url. Meltano could do this for us in `discovery.yml` (and also add some commands).

2. You have to add some `.sqlfluff`/`.sqlfluffignore` files. These could be generated using the `file` Meltano plugin type.

3. You have to be careful about version compatibility with DBT, both within `sqlfluff`'s `pip_url` and between the `sqlfluff` and `dbt` plugins in Meltano. Not sure of a good way to solve this.

4. The DBT project is in `transform/` but `.sqlfluff` needs to be in the current working directory. But that means `sqlfluff` will search your whole project for sql files to lint. Ideally we could have a top-level `.sqlfluffignore` file like:

```gitignore
*
!transform/
```

However this currently doesn't seem to work. In the meantime you'll need to ignore any top-level directories that might have SQL files in them that you don't intend to lint (e.g. `.meltano` contains compiled DBT models).

5. It's a bit slow, as it compiles templates using DBT on every lint run. This in my opinion is still better than trying to mock out every macro call just to get linting to work.

---

See an example of the transform after linting: [293f147ce](https://gitlab.com/rabidaudio/meltano-sqlfluff-example/-/commit/293f147ce54b31f7342c6799ad4562b45a7184e8)

```sql
SELECT * FROM avg_daily_england_intensity;
```

![chart](chart.png)
